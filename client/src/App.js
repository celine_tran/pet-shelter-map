import ShelterMapApp from './components/ShelterMapApp';

/**
 * This class renders the Map Application component by calling ShelterMapApp
 * @returns div, the div contains the ShelterMapApp components
 */
function App() {
  return (
    <div className="App">
      <ShelterMapApp />
    </div>
  );
}

export default App;
