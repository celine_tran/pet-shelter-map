/* eslint-disable indent */
import {
    useMapEvents,
} from "react-leaflet";

/**
 * This is component that invokes a callback of your main Map component 
 * whenever the map is panned or zoomed, passing in the new bounds. 
 * @param {*} props the props passed in from the parent
 * @returns null when there is no action
 * @author Jaya Nilankantan
 */
//react-leaflet hook
function MapMove(props) {
    const mapEvents = useMapEvents({
        "moveend": () => {
            props.action(mapEvents.getBounds());
        },
        "zoom": () => {
            props.action(mapEvents.getBounds());
        },
    });
    return null
}

export default MapMove;