/* eslint-disable indent */
import { Component } from 'react'
import {
  MapContainer,
  TileLayer,
  CircleMarker,
  Popup,
} from "react-leaflet";
import MarkerClusterGroup from "react-leaflet-markercluster";
import "leaflet/dist/leaflet.css";
import "react-leaflet-markercluster/dist/styles.min.css";
import ShelterTooltip from "./ShelterTooltip";
// import MapMove from './MapMove';

/**
 * This class is used to create the map and the markers are rendered.
 */
export default class ShelterMap extends Component {
  constructor(props) {
    super(props);

    this.state = {
      points: [],
      selected: null
    }
    // this.setBounds = this.setBounds.bind(this);
  }

  /**
   * function is called when the component is mounted, it called the fetchShelters function
   */
  async componentDidMount() {
    await this.fetchShelters();
  }

  /**
   * function is called whenever the user moves or zooms in the map.
   * It will call the fetchShelters function when 
   * the previous props is different than the current one
   * @param {*} prevProps the previous props
   */
  // async componentDidUpdate(prevProps) {
  //   if (prevProps.longitude1 !== this.props.longitude1 && 
  //     prevProps.latitude1 !== this.props.latitude1 &&
  //     prevProps.longitude2 !== this.props.longitude2 &&
  //     prevProps.latitude2 !== this.props.latitude2) {
  //     await this.fetchShelters();
  //   }
  // }

  /**
   * function used to fetch data from the server and update the state with the data.
   * @returns nothing when their is an 404 error
   */
  async fetchShelters() {
    try {
      // let shelters = await fetch("/rectangle/?longitude1=" + 
      // this.props.longitude1 + "&longitude2=" + this.props.longitude2 + 
      // "&latitude1=" + this.props.latitude1 + "&latitude2=" + this.props.latitude2);
      let shelters = await fetch("/allShelters");
      let sheltersJson = await shelters.json();
      if (shelters.statusCode === 404) {
        console.log(shelters.statusMessage);
        return;
      }
      this.setState({ points: sheltersJson });

    } catch (e) {
      console.log("Error while fetching");

    }
  }

  /**
   * function used to pass an object to the parent when the map is moved or zoomed in
   * @param {*} object the object from the child MoveMap component.
   */
  // setBounds(object){
  //   this.props.setBounds(object);
  // }


  //render method of the Map component will display a basic map.
  render() {

    return (
      <div>
        <MapContainer
          center={this.props.center}
          zoom={this.props.initialZoom}
          minZoom={this.props.minZoom}
          maxZoom={this.props.maxZoom}
          zoomControl={false}
          updateWhenZooming={false}
          updateWhenIdle={true}
          preferCanvas={true}
          style={{ width: "100%", position: "absolute", top: 0, bottom: 0, zIndex: -1, }}
        >
          <TileLayer
            attribution={this.props.attribution}
            url={this.props.titleUrl}
          />

          <MarkerClusterGroup
            spiderfyOnMaxZoom={false}
            zoomToBoundsOnClick={true}
            showCoverageOnHover={true}
            removeOutsideVisibleBounds={false}
            disableClusteringAtZoom={18}>
            {this.state.points.map((item, index) =>
              <CircleMarker
                key={index}
                color={"blue"}
                opacity={1}
                radius={5}
                weight={1}
                center={[item.geo.coordinates[1], item.geo.coordinates[0]]}
                eventHandlers={{
                  click: () => {
                    this.setState({ selected: item });
                  },
                }
                } />

            )}
          </MarkerClusterGroup>

          {!(this.state.selected === null) &&
            <Popup
              position={[this.state.selected.geo.coordinates[1],
              this.state.selected.geo.coordinates[0]]}
              onClose={() => {
                this.setState({ selected: null });
              }

              }>
              <ShelterTooltip
                name={this.state.selected.name}
                address={this.state.selected.address}
                city={this.state.selected.city}
                phone={this.state.selected.phone}
                email={this.state.selected.email}
                position={this.state.selected.geo.coordinates}
              >
              </ShelterTooltip>
            </Popup>
          }
          {/* <MapMove
            action={this.setBounds}>
          </MapMove> */}

        </MapContainer>
      </div>
    );
  }
}

