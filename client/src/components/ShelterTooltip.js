/* eslint-disable no-tabs */
/* eslint-disable indent */
import { Component } from 'react';
/**
 * This component shows the neccessary information when user clicks on the marker.
 */
export default class ShelterTooltip extends Component {
	
	/**Render returns a div with the name, address, city, phone and email from props */
	render() {
		return (
			<div>
				<b>{this.props.name}</b><br/>
				Address: {this.props.address},
				{this.props.city}<br/>
				Phone: {this.props.phone}<br/>
				Email: {this.props.email}
			</div>
		)
	}
}
