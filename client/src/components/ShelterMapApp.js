import { Component } from 'react'
import config from "../utils/config"
import ShelterMap from "./ShelterMap"

/**
 * The ShelterMapApp class manages the sate of the bounds and updates them
 * everytime the user zooms or moves the map.
 * It initializes its state with the parameters from config.js and passes them to its child.
 */
class ShelterMapApp extends Component {
  constructor(props) {
    super(props)

    this.state = {
      longitude1: config.mapBoundsStarter[0][0],
      latitude1: config.mapBoundsStarter[0][1],
      longitude2: config.mapBoundsStarter[1][0],
      latitude2: config.mapBoundsStarter[1][1],
      center: config.mapCenterStarter,
      titleUrl: config.titleUrl,
      attribution: config.attribution,
      minZoom: config.minZoom,
      maxZoom: config.maxZoom,
      initialZoom: config.initialZoom,
    }
    // this.setBounds = this.setBounds.bind(this);
  }

  // setBounds(object){
  //   this.setState({ 
  //     longitude1: object._southWest.lng,
  //     latitude1: object._southWest.lat,
  //     longitude2: object._northEast.lng,
  //     latitude2: object._northEast.lat
  //   });
  // }

  render() {
    return (
      <div>
        <ShelterMap
          longitude1={this.state.longitude1}
          latitude1={this.state.latitude1}
          longitude2={this.state.longitude2}
          latitude2={this.state.latitude2}
          minZoom={this.state.minZoom}
          maxZoom={this.state.maxZoom}
          initialZoom={this.state.initialZoom}
          center={this.state.center}
          titleUrl={this.state.titleUrl}
          attribution={this.state.attribution}
        >
        </ShelterMap>
      </div>
    )
  }
}

export default ShelterMapApp;
