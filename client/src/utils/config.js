/* eslint-disable indent */
/**
 * This config file is used to set default parameters for the shelter map component.
 */
module.exports = {
    // eslint-disable-next-line max-len
    attribution: "&copy; <a href=\"https://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors",
    titleUrl: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
    mapCenterStarter: [45.7417, -95.3733],
    mapBoundsStarter: [
        [-146.25, 12.554563528593656],
        [-48.515625, 65.5129625532949]
    ],
    minZoom: 1,
    maxZoom: 18,
    initialZoom: 4
};