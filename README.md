# 520-Project-Tran-Tham

This project is a full-stack MERN application with a focus on the visualization of a large dataset of animal shelters on a map, as well as some of the performance considerations.

This project was done in pair programming. Each team member took turns to be the navigator and the driver.
