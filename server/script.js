/* eslint-disable indent */
//Gets the readData from load and the writeToarray from writeToDb 
const writeArray = require("./utils/writeToDb");
const loadData = require("./utils/load.js");

/**
  * @description async function that uses loadData to add all data to the DB
  */
(async() => {
    //load the file
    try {
        let arrayObj = await loadData("./server/utils/petShelters.json");
        await writeArray(arrayObj);
    } catch (error) {
        console.error("there was a problem " + error.message);
    }
    process.exit();
})();

