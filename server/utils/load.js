/* eslint-disable security/detect-non-literal-fs-filename */
/* eslint-disable indent */
const fs = require("fs/promises");

/**
 * @description Async function that uses fs/promise to read a file's contents. 
 * Takes in a JSON file and return an array of objects.
 * @param {JSON} filename A JSON file.
 * @returns {object} The parsed json file into an object of arrays.
 */
async function readData(filename) {
    try {
        let data = await fs.readFile(filename, "utf-8");
        let objArray = JSON.parse(data);
        return objArray;

    } catch (err) {
        console.error("An error occured: " + err);
        throw err;
    }
}

//Export module
module.exports = readData;