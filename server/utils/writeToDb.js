/* eslint-disable indent */
// get db connection
const CONN = require("../db/conn.js");
const db = new CONN();

/**
 * @description Async function that writes the array to to the database by calling its functions.
 * @param {Array} array An array of objects.
 * @return {Object} An index.
 */
async function writeArray(array) {
    //Create new array
    let newArray = [];
    //Loop through each iteration and check if lat and long exist and if they are numbers
    //If so, push its selected data to the new array
    for (var i = 0; i < array.length; i++) {
        let position = array[parseInt(i)];
        if (!(Number.isNaN(position["latitude"]) || Number.isNaN(position["longitude"]))) {
            newArray.push({
                name: position.name,
                address: position.address1,
                city: position.city,
                phone: position.phone,
                email: position.email,
                geo: {
                    "type": "Point",
                    "coordinates": [
                        parseFloat(position.longitude),
                        parseFloat(position.latitude)
                    ]
                }
            })
        }
    }

    try {
        //Connect to database, insertMany and create an index
        await db.connect("db1", "petShelters");
        await db.insertMany(newArray);
        return await db.createIndex({ "geo": "2dsphere" });
    } finally {
        if (db) {
            //Once done, disconnect from database
            db.disconnect();
        }
    }
}

//Export module
module.exports = writeArray;