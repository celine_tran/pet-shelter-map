/* eslint-disable no-tabs */
const express = require("express");
const router = express.Router();
//deconstructing assignment
const { allShelters, shelter, rectangle } = require("./endpoints.js");

// add routes to the Router object
// Home page route.
/**
 * @swagger
 * /allShelters:
 *   get:
 *     summary: Retrieve a list of all shelters
 *     description: Retrieve a list of all shelters from mongoDB in JSON format.
 *     responses:
 *       200:
 *         description: All shelters found
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       _id:
 *                         type: ObjectID
 *                         description: The shelter ID
 *                         example: 61a15797af6d09a741393d64
 *                       name:
 *                         type: string
 *                         description: The shelter's name
 *                         example: Refuge pour chats de Verdun
 *                       address:
 *                         type: string
 *                         description: The shelter's address
 *                         example: PO BOX 28536 STN JEAN-COUTU
 *                       city:
 *                         type: string
 *                         description: The shelter's city
 *                         example: Verdun
 *                       phone:
 *                         type: string
 *                         description: The shelter's phone number
 *                         example: 514-761-4444
 *                       email:
 *                         type: string
 *                         description: The shelter's email
 *                         example: circle
 *                       geo:
 *                         type: object
 *                         properties:
 *                           type:
 *                             type: string
 *                             description: type of the object
 *                             example: Point
 *                           coordinates:
 *                             type: array
 *                             description: Longitude and Latitude 
 *                             example: [-73.568, 45.47]
 */
router.get("/allShelters", allShelters);

/**
 * @swagger
 * /shelter:
 *   get:
 *     summary: Retrieve a shelter by its ID
 *     description: Retrieve a document of a shelter found depending on the given ID in JSON format.
 *     parameters:
 *       - in: query
 *         name: _id
 *         required: true
 *         description: Shelter's ID
 *         schema:
 *           type: string
 *         example: 61a15797af6d09a741393a6e
 *     responses:
 *       200:
 *         description: A shelter found depending of the ID
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       _id:
 *                         type: ObjectID
 *                         description: The shelter ID
 *                         example: 61a15797af6d09a741393d64
 *                       name:
 *                         type: string
 *                         description: The shelter's name
 *                         example: Refuge pour chats de Verdun
 *                       address:
 *                         type: string
 *                         description: The shelter's address
 *                         example: PO BOX 28536 STN JEAN-COUTU
 *                       city:
 *                         type: string
 *                         description: The shelter's city
 *                         example: Verdun
 *                       phone:
 *                         type: string
 *                         description: The shelter's phone number
 *                         example: 514-761-4444
 *                       email:
 *                         type: string
 *                         description: The shelter's email
 *                         example: circle
 *                       geo:
 *                         type: object
 *                         properties:
 *                           type:
 *                             type: string
 *                             description: type of the object
 *                             example: Point
 *                           coordinates:
 *                             type: array
 *                             description: Longitude and Latitude 
 *                             example: [-73.568, 45.47]
 */
router.get("/shelter", shelter);

/**
 * @swagger
 * /rectangle:
 *   get:
 *     summary: Retrieve a list of all shelters in rectangle
 *     description: Retrieve a list of shelters found within a given rectangle coordinates.
 *     parameters:
 *       - in: query
 *         name: longitude1
 *         required: true
 *         description: South-West Longitude
 *         schema:
 *           type: number
 *         example: -74.23187255859375
 *       - in: query
 *         name: latitude1
 *         required: true
 *         description: South-West Latitude
 *         schema:
 *           type: number
 *         example: 45.24201954967741
 *       - in: query
 *         name: longitude2
 *         required: true
 *         description: North-East Longitude
 *         schema:
 *           type: number
 *         example: -72.894287109375
 *       - in: query
 *         name: latitude2
 *         required: true
 *         description: North-East Latitude
 *         schema:
 *           type: number
 *         example: 45.7157686770051
 *     responses:
 *       200:
 *         description: All shelters within a given rectangle
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       _id:
 *                         type: ObjectID
 *                         description: The shelter ID
 *                         example: 61a15797af6d09a741393d64
 *                       name:
 *                         type: string
 *                         description: The shelter's name
 *                         example: Refuge pour chats de Verdun
 *                       address:
 *                         type: string
 *                         description: The shelter's address
 *                         example: PO BOX 28536 STN JEAN-COUTU
 *                       city:
 *                         type: string
 *                         description: The shelter's city
 *                         example: Verdun
 *                       phone:
 *                         type: string
 *                         description: The shelter's phone number
 *                         example: 514-761-4444
 *                       email:
 *                         type: string
 *                         description: The shelter's email
 *                         example: circle
 *                       geo:
 *                         type: object
 *                         properties:
 *                           type:
 *                             type: string
 *                             description: type of the object
 *                             example: Point
 *                           coordinates:
 *                             type: array
 *                             description: Longitude and Latitude 
 *                             example: [-73.568, 45.47]
 */
router.get("/rectangle", rectangle);


module.exports = router;