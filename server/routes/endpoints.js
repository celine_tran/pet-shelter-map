/* eslint-disable indent */
const CONN = require("../db/conn");
const db = new CONN();
const ObjectId = require("mongodb").ObjectId;
const cache = require("memory-cache");

/**
 * @description AN async function which is an endpoint that returns all documents.
 * @param req The request 
 * @param res The result
 */
async function allShelters(req, res) {
    try {
        //get data
        let data = cache.get("getAll");
        //if not cache. cache the data
        if (!data) {
            data = await db.findAll();
            cache.put("getAll", data);
        }
        //send data
        res.contentType("application/json");
        res.send(data);
    } catch (e) {
        console.error(e.message);
        res.sendStatus(500).end();
    }
}

/**
 * @description async function which is an endpoint that returns all fields on a particular _id.
 * example: http://localhost:3001/shelter/?_id=61a15797af6d09a741393a6e
 * @param req The request
 * @param res The result
 */
async function shelter(req, res) {
    try {
        //Check is each parameters in the rectangle is passed in the query
        //else send 404 error
        if (req.query._id) {
            //get data in cache
            let data = cache.get(req.query._id);
            //if not in cache, cache the data
            if (!data) {
                data = await db.findOne({ "_id": new ObjectId(req.query._id) });
                cache.put(req.query._id, data);
            }
            //send data
            res.contentType("application/json");
            res.send(data);
        } else {
            res.sendStatus(404).end();
        }
    } catch (e) {
        console.error(e.message);
        res.sendStatus(500).end();
    }
}


/**
 * @description async function which is an endpoint that returns all the 
 * documents whose points are found within a given rectangle.
 * Example: http://localhost:3001/rectangle/?longitude1=-74.23187255859375
 * &longitude2=-72.894287109375&latitude1=45.24201954967741&latitude2=45.7157686770051
 * @param req The request
 * @param res The result
 */
async function rectangle(req, res) {
    try {
        //Check is each parameters in the rectangle is passed in the query
        //else send 404 error
        if (parseFloat(req.query.longitude1) &&
            parseFloat(req.query.longitude2) &&
            parseFloat(req.query.latitude1) &&
            parseFloat(req.query.latitude2)) {

            //get data in cache
            let data = cache.get(req.query.longitude1 + req.query.longitude2 + 
                req.query.latitude1 + req.query.latitude2);
            //if not in cache, cache it
            if (!data) {
                //call findGeo() to find all data
                data = await db.findGeo(parseFloat(req.query.longitude1),
                    parseFloat(req.query.latitude1),
                    parseFloat(req.query.longitude2),
                    parseFloat(req.query.latitude2));

                cache.put(req.query.longitude1 + req.query.longitude2 +
                    req.query.latitude1 + req.query.latitude2, data);
            }
            //send data
            res.contentType("application/json");
            res.send(data);
        } else {
            res.sendStatus(404).end();
        }
    } catch (e) {
        console.error(e.message);
        res.sendStatus(500).end();
    }
}

//Export module
module.exports = {
    allShelters,
    shelter,
    rectangle
}