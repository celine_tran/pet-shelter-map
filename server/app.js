/* eslint-disable indent */
const express = require("express");
const app = express();
const router = require("./routes/router.js");
const path = require("path");

//swagger required modules
const swaggerJSDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");

const swaggerDefinition = {
  openapi: "3.0.0",
  info: {
    title: "Express API for Pets Shelters in North America",
  },
};

const options = {
  swaggerDefinition,
  // Paths to files containing OpenAPI definitions
  apis: ["./server/routes/*.js"],
};
const swaggerSpec = swaggerJSDoc(options);

//add compression middleware
const compression = require("compression");
app.use(compression());

//anything in the URL path / uses the Router
app.use("/", router);

///other routes
app.use(express.static(path.resolve(__dirname, "../client/build")));

//endpoint to /docs tobe able to navigate to Swagger docs page
app.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerSpec));

//middleware for browser caching
app.use(function(req, res, next) {
  res.set("Cache-control", "public, max-age=31536000");
  next();
});

//Export module
module.exports = app;