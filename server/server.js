/* eslint-disable indent */
const conn = require("./db/conn");
const app = require("./app");
const PORT = process.env.PORT || 3001;

/**
 * @description async function that uses connect to the DB and listen to a port.
 * It only starts listening after connection in place
 */
(async() => {
    try {
        const db = new conn();
        await db.connect("db1", "petShelters");

    } catch (e) {
        console.error("could not connect");
        process.exit();
    }
    app.listen(PORT, () => {
        console.log(`Server is listening on port ${PORT}!`);
    });
})();