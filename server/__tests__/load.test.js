const readJSON = require("../utils/load");

describe("Test load class", () => {
  /*test for a file found and parsed sucessfully*/
  test("Test case 1, find file and parse it successfully", async() => {
    const fileName = "__tests__/testCase1.json";
    const returnJSON = await readJSON(fileName);
    await expect(returnJSON["Mercury"]).toBe(2440);
  });

  /*test for file that is not in json format (the error message will contain “Unexpected”)*/
  test("Test case 2, find file and parse it with incorrect format", async() => {
    const fileName = "__tests__/testCase2.txt";
    await expect(readJSON(fileName)).rejects.toThrow("Unexpected");
  });


  /*test for a file not found (the error message will contain “no such”)*/
  test("Test case 3, file not found and parse it", async() => {
    const fileName = "testCase.json";
    await expect(readJSON(fileName)).rejects.toThrow("no such");
  });

});