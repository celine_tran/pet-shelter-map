const request = require("supertest");
const app = require("../app");
const CONN = require("../db/conn");

jest.mock("../db/conn");

describe("GET / ", () => {
  test("findOne should respond with a json object", async () => {

    jest.spyOn(CONN.prototype, "findOne").mockResolvedValue(
      { "_id": "61a13defcda3fc47a16c2c3a", "name": "Refuge pour chats de Verdun"});
    
    const response = await request(app).get("/shelter").query(
      { "_id": "61a13defcda3fc47a16c2c3a" });
    
    expect(response.body).toEqual(
      { "_id": "61a13defcda3fc47a16c2c3a", "name": "Refuge pour chats de Verdun"});
    expect(response.statusCode).toBe(200);
    expect(response.type).toEqual("application/json");
  });

  test("findOne should respond with an 404 error when no query", async () => {

    jest.spyOn(CONN.prototype, "findOne").mockResolvedValue(
      { "_id": "61a13defcda3fc47a16c2c3a", "name": "Refuge pour chats de Verdun"});
    
    const response = await request(app).get("/shelter").query();
 
    expect(response.statusCode).toBe(404);
  });

  test("findAll should respond with a json", async () => {
    
    jest.spyOn(CONN.prototype, "findAll").mockResolvedValue(
      {"name": "Refuge pour chats de Verdun"});
        
    const response = await request(app).get("/allShelters");
        
    expect(response.body).toEqual({"name": "Refuge pour chats de Verdun"});
    expect(response.statusCode).toBe(200);
    expect(response.type).toEqual("application/json");
  });

  test("rectangle should respond with a json", async () => {
    
    jest.spyOn(CONN.prototype, "findGeo").mockResolvedValue(
      {"name": "Refuge pour chats de Verdun"});
    
    const response = await request(app).get("/rectangle").query(
      { "longitude1":"-74.23187255859375", "longitude2": "-72.894287109375",
        "latitude1":"45.24201954967741", "latitude2":"45.7157686770051"});
    
    expect(response.body).toEqual({"name": "Refuge pour chats de Verdun"});
    expect(response.statusCode).toBe(200);
    expect(response.type).toEqual("application/json");
  });

  test("rectangle should respond with an 404 error when no query", async () => {
    
    jest.spyOn(CONN.prototype, "findGeo").mockResolvedValue(
      {"name": "Refuge pour chats de Verdun"});
    
    const response = await request(app).get("/fieldsByLonLat").query();

    expect(response.statusCode).toBe(404);
  });

  
});