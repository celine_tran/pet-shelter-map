/* eslint-disable indent */
//Database url from Atlas taken from .env
require("dotenv").config();
const dbUrl = process.env.ATLAS_URI;
const { MongoClient } = require("mongodb");
let instance = null;


/**
 * @description This file is a module that provides the required database functions of phase 1.
 */
class CONN {
    /**
     * @description A constructor that creates a new mongoClient,initializes collection and
     *  other elements, then returns the instance.
     */
    constructor() {
        if (!instance) {
            instance = this;
            this.client = new MongoClient(dbUrl);
            this.db = null;
            this.collection = null;
        }
        return instance;
    }


    /**
     * @description Function creates an index.
     * @param {Object} object A passed in object.
     */
    async createIndex(object) {
        await this.collection.createIndex(object);
    }


    /**
     * @description Method connects to database and creates a new collection.
     * @param dbname The database name.
     * @param collName The collection name.
     */
    async connect(dbname, collName) {
        if (this.db) {
            return;
        }
        await this.client.connect();
        this.db = await this.client.db(dbname);
        console.log("Successfully connected to MongoDB database " + dbname);
        this.collection = await this.db.collection(collName)
    }


    /**
     * @description Function inserts the array of many data into a collection.
     * @param {Array} array An array of selected data.
     * @return {Object} An object with information about the insert operations.
     */
    async insertMany(array) {
        let result = await this.collection.insertMany(array);
        return result.insertedCount;
    }


    /**
     * @description Function gets all documents.
     * @return {Object} An object with all information about the operations.
     */
    async findAll() {
        let result = await this.collection.find().project({});
        return result.toArray();
    }


    /**
     * @description Function gets one document.
     * @param {Query} query A querry of selected data.
     * @return {Object} An object with all information about the operations.
     */
    async findOne(query) {
        let result = await this.collection.findOne(query);
        return result;
    }


    /**
     * @description Function gets all documents whose points are found within a given rectangle
     * @param {String} longitude1 north-east longitude
     * @param {String} latitude1 north-east latitude
     * @param {String} longitude2 south-westlongitude
     * @param {String} latitude2 south-west latitude
     * @return {Object} An object with all information about the operations.
     */
    async findGeo(longitude1, latitude1, longitude2, latitude2) {
        let result = await this.collection.find({
            geo: {
                $geoWithin: {
                    $geometry: {
                        type: "Polygon",
                        coordinates: [
                            [
                                [longitude1, latitude1],
                                [longitude2, latitude1],
                                [longitude2, latitude2],
                                [longitude1, latitude2],
                                [longitude1, latitude1]
                            ]
                        ]
                    }
                }
            }
        });
        return result.toArray();
    }


    /**
     * @description Function disconnects from the database.
     */
    async disconnect() {
        this.client.close();
    }
}

//Export module
module.exports = CONN;